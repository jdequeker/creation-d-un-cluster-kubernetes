#!/bin/bash

## Inspired from https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/

# From the master node, we want to deploy an nginx-container
echo "Deploying an nginx-container from the web..."
kubectl apply -f https://k8s.io/examples/application/deployment.yaml

echo "To get pod description"
kubectl describe deployment nginx-deployment

echo "List the pods created for nginx"
kubectl get pods -l app=nginx


echo "To expose the service"
kubectl expose deployment nginx-deployment --type=LoadBalancer --name=my-service
