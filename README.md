
# Kubernetes cluster

This repository helps to [build a Kubernetes cluster](README.md#build-the-cluster) of 1 master and 2 slaves.
It depends on vagrant to deploy the virtual machines.

Command lines are given for the following steps:

* [Create VMs for the Kubernetes cluster](README.md#build-the-cluster)
* [Install the dashboard](README.md#install-the-dashboard)
* [Deploy a container](README.md#deploy-a-container)
* [Deploy a container using terraform](README.md#deploy-a-container-using-terraform)
* [Access to the pod from the host machine](README.md#get-an-external-ip)

## Build the cluster
### Start the VMs
The `vagrant up` command is to be used to create (if not exists) or run the machines.
It relies on the Vagrantfile which also contain informations to install kubernetes and docker stuff.
### Destroy a VM
To destroy a VM, just `vagrant destroy machine-name` (where `machine-name` is "master", "node01" or "node02"), or `vagrant destroy` for all the cluster.
### Connect to a machine
Just `vagrant ssh machine-name`.
### Switch off the cluser
To switch off the cluster, the command is `vagrant halt`.

## Install the dashboard
The dashboard comes from: https://github.com/kubernetes/dashboard and is already install with the Vagrantfile.

To access to the dashboard, just go to http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
Informations to create a granted user and to generate a token can be found there: https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
Because we work with virtual machine, we need to:
1.  Install kubectl on the host machine (depending on the host OS). Do not forget to update the *path*.
2.  Get the config file from the master node (`cp ~/.kube/config /vagrant/.`) to the host (`cp config ~/.kube/config`)
3.  Run `kubectl proxy` on the host

The token can be get with `kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')` on the master.

## Deploy a container
To deploy a containter, an example is given with the *deployment.sh* file.
```
vagrant ssh master # Connect to the master
cd /vagrant # Go to the shared directory
bash deployment.sh # Run the script to deploy (apply the yaml file) an nginx container over 2 pods and create a service to expose it
```

## Deploy a container using terraform
Once terraform is installed (and the *~/.kube/conf* copied from the master to the host),
```
terraform init # For the first time
terraform plan 
terraform apply
```
will read the *terraform.tf* file and deploy an nginx docker image (*nginxdemos/hello*) over 2 pods, with a loadbalancer.

As *nginxdemos/hello* shows the host name and the host ip address, with several connections to the public IP, we can see how the loadbalancer works.

## Get an external IP
Everything is already done and should work (thanks to instructions in the Vagrantfile).
However, informations can be found there: https://metallb.universe.tf/tutorial/layer2/

