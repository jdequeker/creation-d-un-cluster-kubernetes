# Vagrant file for a kubernetes cluster
#  containing 1 master and 2 nodes

BOX_IMAGE = "ubuntu/xenial64"
NODE_COUNT = 2

Vagrant.configure("2") do |config|

  config.vm.provider "virtualbox" do |v|
    v.memory = 1024
    v.cpus = 1
  end

  config.vm.provision "shell", inline: <<-SHELL
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list.d/kubernetes.list
    apt-get update
    apt-get install -y kubelet kubeadm kubectl docker.io
    apt-mark hold kubelet kubeadm kubectl docker.io  
    cat /vagrant/hosts_2_add_2_etc-hosts.lst >> /etc/hosts
  SHELL

  config.vm.define "master" do |subconfig|
    subconfig.vm.box = BOX_IMAGE
    subconfig.vm.hostname = "c1-master1"
    subconfig.vm.network :private_network, ip: "172.16.94.10"
    subconfig.vm.provider "virtualbox" do |vb|
      vb.memory = 2048
      vb.cpus = 2
    end
#    subconfig.vm.memory = 2048
    subconfig.vm.provision "shell", inline: <<-SHELL
      sudo kubeadm init --pod-network-cidr=192.168.0.0/16 --apiserver-advertise-address=172.16.94.10 > /vagrant/join_token.txt
      exit
    SHELL

    subconfig.vm.provision "shell", privileged: false, inline: <<-SHELL
#      whoami
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config  
      wget https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
      wget https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
      kubectl apply -f rbac-kdd.yaml # gestion des rôles
      kubectl apply -f calico.yaml # gestion de l'overlay network
      kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml # pour le dashboard
      kubectl apply -f /vagrant/dashboard-adminuser.yaml # Pour créer un admin user qui accedera au dashboard
      kubectl apply -f /vagrant/dashboard-adminuser2.yaml # Pour créer un admin user qui accedera au dashboard
      kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml # Pour l'accès depuis l'host -> proxy
      kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/example-layer2-config.yaml # Pour configurer l'accès depuis l'host
    SHELL
  end
  
  (1..NODE_COUNT).each do |i|
    config.vm.define "node0#{i}" do |subconfig|
      subconfig.vm.box = BOX_IMAGE
      subconfig.vm.hostname = "c1-node#{i}"
      subconfig.vm.network :private_network, ip: "172.16.94.#{i + 10}"
      subconfig.vm.provision "shell", inline: <<-SHELL
        grep "kubeadm join" /vagrant/join_token.txt | sh -
      SHELL
    end
  end
  
end
